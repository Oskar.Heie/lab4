package cellular;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

    @Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
    public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i < numberOfRows(); i++) {
            for (int j = 0; j < numberOfColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration;
	}

    @Override
    public CellState getNextCell(int row, int col) {
        if (getCellState(row, col).equals(CellState.ALIVE)) {
            return CellState.DYING;
        }
        if (getCellState(row, col).equals(CellState.DEAD)) {
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {
                return CellState.ALIVE;
            }
        }
        return CellState.DEAD;
    }

    @Override
    public int numberOfRows() {
		return currentGeneration.numRows();
	}

    @Override
    public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

    private int countNeighbors(int row, int col, CellState state) {
		int counter = 0;
		for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
				if ((i == 0) && (j == 0)) {
					continue;
				}
				// checks if in grid
				else if ((row + i < 0) || (col + j < 0) || (row + i >= numberOfRows()) || (col + j >= numberOfColumns())) {
					continue;
				} 
				else if (getCellState(row+i, col+j).equals(CellState.ALIVE)) {
					counter++;
				}
			}
		}
		return counter;
	}

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}
